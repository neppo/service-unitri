package br.edu.unitri.service.controller

import br.edu.unitri.service.domain.Student
import br.edu.unitri.service.domain.TestInput
import br.edu.unitri.service.service.StudentService
import io.micrometer.core.instrument.MeterRegistry
import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.Duration
import javax.persistence.EntityManager
import javax.persistence.ParameterMode



@RestController
@RequestMapping("performance")
class PeformanceController @Autowired constructor(val entityManager: EntityManager,
                                                  val metricsRegistry: MeterRegistry,
                                                  val service: StudentService) {

    companion object : KLogging()

    @GetMapping("/test1")
    fun hit(@RequestBody data: TestInput): Long {


        val init = System.currentTimeMillis()

        val query = entityManager
                .createStoredProcedureQuery("PS_TESTE_DESEMPENHO")
                .registerStoredProcedureParameter(
                        1,
                        Long::class.java,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        2,
                        Long::class.java,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        3,
                        Long::class.java,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        4,
                        Long::class.java,
                        ParameterMode.IN
                )
                .setParameter(1, data.session)
                .setParameter(2, data.records)
                .setParameter(3, data.iterations)
                .setParameter(4, data.commits)

        query.execute()

        metricsRegistry.timer("performance.times").record(Duration.ofMillis(System.currentTimeMillis() - init))

        return System.currentTimeMillis() - init

    }

    @GetMapping("/test2")
    fun hit2(@RequestBody data: TestInput): Long {


        val init = System.currentTimeMillis()
        //save
        for (i in 1..(data.iterations ?: 10)) {
            for (r in 1..(data.records ?: 10)) {
                val student = Student()
                student.studentCode = "code $i-$r"
                student.name = "name $i-$r"
                student.citycode = i
                service.saveStudent(student)
            }
        }

        //update
        for (i in 1..(data.iterations ?: 10)) {
            for (r in 1..(data.records ?: 10)) {
                val student = Student()
                student.studentCode = "code $i-$r"
                student.citycode = i + 1
                service.saveStudent(student)
            }
        }
        //remove
        for (i in 1..(data.iterations ?: 10)) {
            for (r in 1..(data.records ?: 10)) {
                val student = Student()
                student.studentCode = "code $i-$r"
                student.citycode = i + 1
                service.deleteStudent(student)
            }
        }

        metricsRegistry.timer("performance2.times").record(Duration.ofMillis(System.currentTimeMillis() - init))

        return System.currentTimeMillis() - init

    }

}