package br.edu.unitri.service.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import java.util.Objects

@Entity
@Table(name = "TB_STUDENT")
class Student {

    @Id
    @Column(name = "studentcode")
    var studentCode: String? = null

    @Column(name = "name")
    var name: String? = null

    @Column(name = "address")
    var address: String? = null

    @Column(name = "citycode")
    var citycode: Long? = null

    @Column(name = "memo")
    var memo: Long? = null

    override fun toString(): String {
        return "Student{" +
                "studentCode='" + studentCode + '\''.toString() +
                ", name='" + name + '\''.toString() +
                ", address='" + address + '\''.toString() +
                ", citycode=" + citycode +
                ", memo=" + memo +
                '}'.toString()
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val student = o as Student?
        return studentCode == student!!.studentCode
    }

    override fun hashCode(): Int {

        return Objects.hash(studentCode)
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
