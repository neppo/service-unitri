package br.edu.unitri.service.domain

class TestInput {

    var session: Long? = null
    var records: Long? = null
    var commits: Long? = null
    var iterations: Long? = null

}