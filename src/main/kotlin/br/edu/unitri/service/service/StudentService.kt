package br.edu.unitri.service.service

import br.edu.unitri.service.domain.Student
import br.edu.unitri.service.repository.StudentRepository
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty
import io.micrometer.core.instrument.MeterRegistry
import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class StudentService @Autowired constructor(val repository: StudentRepository, val metricsRegistry: MeterRegistry) {

    companion object : KLogging()

    fun countStudents(): Long {

        return repository.count()
    }

    fun getStudent(): List<Student> {

        return repository.findAll().toList()
    }

    @HystrixCommand(fallbackMethod = "saveStudentFallback", commandProperties = [
        (HystrixProperty(name = "circuitBreaker.enabled", value = "true")),
        (HystrixProperty(name = "execution.timeout.enabled", value = "true")),
        (HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1500")),
        (HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10"))

    ])
    fun saveStudent(student: Student) {

        metricsRegistry.counter("student.save").increment()
        repository.save(student)
    }

    fun saveStudentFallback(student: Student, e: Throwable) {

        logger.info(e) { "erro ao gravar estudante $student" }
        metricsRegistry.counter("student.save.fallback").increment()
    }

    fun getStudentByCode(id: String): Optional<Student> {

        return repository.findById(id)
    }

    fun deleteStudent(student: Student) {

        repository.delete(student)
    }
}