                          
# Springboot microservice - Kotlin

 - Rest services
 - Spring data / jpa
 - Oracle
 - Circuit breaker - Hystrix
 - Metrics - Micrometer
 - ...
 
 ## Build
 
 `
./gradlew clean build
`
                           
## Build com teste
 
  `
 ./gradlew clean build test
`

O artefato executável será gerado em

`
build/libs/service-unitri.jar
`

## Executar o serviço pelo Java

`
java -jar build/libs/service-unitri.jar
`

## Executar o serviço utilizando o gradle wrapper

`
./gradlew bootRun
`